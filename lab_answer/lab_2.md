1. Apakah perbedaan antara JSON dan XML?

JSON dan XML adalah format yang digunakan untuk pertukaran data. Kedua format ini menerima data dari web server. Walaupun memiliki kegunaan yang sama, XML dan JSON juga memiliki perbedaan. Perbedaan pertama antara kedua format ini terletak pada kemudahan membaca. Data pada XML akan disimpan sebagai tree structure yang menyebabkan penggunaan tag dalam strukturnya. Hal ini menyebabkan XML lebih sulit untuk dibaca. Sedangkan, data pada JSON akan disimpan sebagai map dengan pasangan key value sehingga terlihat rapih dan mudah dibaca. Kemudian, perbedaan JSON dan XML juga terletak pada tipe data yang didukung. XML mendukung penggunaan tipe data kompleks seperti bagan, charts, gambar, number, teks, serta menyediakan opsi untuk mentransfer struktur atau format data dengan data aktual. Di lain sisi, JSON hanya mendukung penggunaan string, tipe data yang berhubungan dengan angka, array, boolean, dan objek yang hanya dapat berisi tipe data primitif. Dalam hal kecepatan transfer data, JSON cenderung lebih cepat karena ukuran file yang dihasilkan sangat kecil serta penguraian data juga lebih cepat oleh mesin JavaScript. Sementara itu, file XML besar dan lambat dalam penguraian data sehingga transmisi data menjadi lebih lambat. 


2. Apakah perbedaan antara HTML dan XML?

HTML dan XML adalah bahasa markup dengan fokus yang berbeda. HTML atau Hypertext Markup Language berfokus pada penyajian data. Sedangkan XML atau Extensible Markup Language memiliki fokus pada transfer data. Dalam menulis kode, HTML dan XML juga memiliki perbedaan seperti pada kepekaan besar kecilnya huruf yang digunakan. HTML merupakan bahasa yang case insensitive yaitu tidak memperhatikan besar kecilnya huruf yang digunakan. Sebaliknya, XML merupakan bahasa yang case sensitive sehingga perbedaan pada besar kecilnya huruf dapat memengaruhi keseluruhan penulisan kode. Perbedaan lainnya saat melakukan penulisan kode yaitu pada penggunaan tag penutup. Kedua bahasa ini menggunakan tag dalam strukturnya. Namun, tidak semua tag dalam HTML membutuhkan penutup seperti pada tag <br> yang tidak membutuhkan </br>. Hal ini berbanding terbalik dengan XML yang sangat ketat dalam menggunakan tag penutup.

Referensi :
https://www.monitorteknologi.com/perbedaan-json-dan-xml/
https://www.guru99.com/json-vs-xml-difference.html#7
https://dosenit.com/kuliah-it/pemrograman/perbedaan-xml-dan-html#Beberapa_kunci_perbedaan_yang_dapat_ditemukan
https://www.guru99.com/xml-vs-html-difference.html#7
