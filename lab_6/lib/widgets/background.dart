import 'package:flutter/material.dart';

class Background extends StatelessWidget {
  final Widget child;

  const Background({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Stack(
      children: [
      Scaffold(
        backgroundColor: Color(0xFFE0E1DD),
        body: Column(
          children: <Widget>[
          Align(
            alignment: Alignment(-0.9, -0.9),
            child: Text(
                'Kladaf',
                style: TextStyle(
                  fontFamily: 'Roboto',
                  color: Color(0xFF0D1B2A),
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ),
      child
    ]);
  }
}
